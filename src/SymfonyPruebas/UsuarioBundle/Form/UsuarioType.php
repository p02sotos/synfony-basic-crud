<?php

namespace SymfonyPruebas\UsuarioBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UsuarioType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username',null,array('attr'=>array(
        'class'=>'form-control'
    )))
            ->add('email','email',array('attr'=>array(
                'class'=>'form-control'
            )))
            ->add('password', 'repeated', array(
                'type' => 'password',
                'invalid_message' => 'Las dos contraseñas deben coincidir',
                'options' => array(
                    'label' => 'Contraseña',
                    'attr'=>array('class'=>'form-control')),
            ))        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SymfonyPruebas\UsuarioBundle\Entity\Usuario'
        ));
    }

    public function getName()
    {
        return 'symfonypruebas_usuariobundle_usuariotype';
    }
}
