<?php

namespace SymfonyPruebas\UsuarioBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PropietarioType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre',null, array('attr'=>array(
                'class'=>'form-control'
            )))
            ->add('apellidos',null, array('attr'=>array(
                'class'=>'form-control'
            )))
            ->add('edad',null, array('attr'=>array(
                'class'=>'form-control'
            )))
            ->add('dni',null, array('attr'=>array(
                'class'=>'form-control'
            )))
            ->add('direccion',null, array('attr'=>array(
                'class'=>'form-control'
            )))
            ->add('notas',null, array('attr'=>array(
                'class'=>'form-control'
            )))
            ->add('cuenta',null, array('attr'=>array(
                'class'=>'form-control'
            )))

        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SymfonyPruebas\UsuarioBundle\Entity\Propietario'
        ));
    }

    public function getName()
    {
        return 'symfonypruebas_usuariobundle_propietariotype';
    }
}
