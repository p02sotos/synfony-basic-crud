<?php

namespace SymfonyPruebas\UsuarioBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ComunidadType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre',null, array('attr'=>array(
                'class'=>'form-control'
            )))
            ->add('cif',null, array('attr'=>array(
                'class'=>'form-control'
            )))
            ->add('presidente',null, array('attr'=>array(
                'class'=>'form-control'
            )))
            ->add('direccion',null, array('attr'=>array(
                'class'=>'form-control'
            )))
            ->add('descripcion',null, array('attr'=>array(
                'class'=>'form-control'
            )))
            ->add('notas',null, array('attr'=>array(
                'class'=>'form-control'
            )))

        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SymfonyPruebas\UsuarioBundle\Entity\Comunidad'
        ));
    }

    public function getName()
    {
        return 'symfonypruebas_usuariobundle_comunidadtype';
    }
}
