<?php

namespace SymfonyPruebas\UsuarioBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * ComunidadRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ComunidadRepository extends EntityRepository
{

    public function findComunidad($usuario_id){
        $em = $this->getEntityManager();
        $consulta = $em->createQuery('
                SELECT c
                FROM UsuarioBundle:Comunidad c
                JOIN c.usuario u
                WHERE u.id = :id
                ');
        $consulta->setMaxResults(1);
        $consulta->setParameter('id', $usuario_id);
        return $consulta->getSingleResult();


    }
}
