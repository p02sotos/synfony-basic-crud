<?php

namespace SymfonyPruebas\UsuarioBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use SymfonyPruebas\UsuarioBundle\Entity\Comunidad;
use SymfonyPruebas\UsuarioBundle\Entity\Propietario;
use SymfonyPruebas\UsuarioBundle\Entity\Usuario;
use SymfonyPruebas\UsuarioBundle\Form\ComunidadType;
use SymfonyPruebas\UsuarioBundle\Form\PropietarioType;
use SymfonyPruebas\UsuarioBundle\Form\UsuarioType;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class DefaultController extends Controller
{

    public function loginAction()
    {
        $peticion = $this->getRequest();
        $sesion = $peticion->getSession();

        $error = $peticion->attributes->get(
            SecurityContext::AUTHENTICATION_ERROR,
            $sesion->get(SecurityContext::AUTHENTICATION_ERROR)
        );

        return $this->render('UsuarioBundle:Default:login.html.twig', array(
            'last_username' => $sesion->get(SecurityContext::LAST_USERNAME),
            'error' => $error
        ));
    }

    public function registroAction()
    {
        $peticion = $this->getRequest();
        $sesion = $peticion->getSession();
        $em = $this->getDoctrine()->getManager();

        $error = $peticion->attributes->get(
            SecurityContext::AUTHENTICATION_ERROR,
            $sesion->get(SecurityContext::AUTHENTICATION_ERROR)
        );

        $usuario = new Usuario();
        $comunidad = new Comunidad();

        $usuario->setSalt('');
        $formulario = $this->createForm(new UsuarioType(), $usuario);
        if ($peticion->getMethod() == 'POST') {
            $formulario->bind($peticion);
            // Validar los datos enviados y guardarlos en la base de datos
            if ($formulario->isValid()) {
                $token = new UsernamePasswordToken(
                    $usuario,
                    $usuario->getPassword(),
                    'usuarios',
                    $usuario->getRoles()
                );
                $this->container->get('security.context')->setToken($token);
                $comunidad->setUsuario($usuario);
                $comunidad->setNombre('');
                $comunidad->setCif('');
                $comunidad->setDescripcion('');
                $comunidad->setDireccion('');
                $comunidad->setNotas('');
                $comunidad->setPresidente('');
                $em->persist($comunidad);
                $em->persist($usuario);
                $em->flush();
                $this->get('session')->setFlash('info', '¡Enhorabuena! Te has registrado correctamente en Cupon, Por favor, introduce los datos para entrar');
                $sesion->set('usuario_id', $usuario->getId());
                return $this->redirect($this->generateUrl('edit_comunidad'));

            }
        }
        return $this->render('UsuarioBundle:Default:registro.html.twig',
            array(
                'edit'=> false,
                'ruta' => 'registro',
                'usuario' => $usuario,
                'formulario' => $formulario->createView()
            ));
    }

    public function editUserAction()
    {
        $peticion = $this->getRequest();
        $sesion = $peticion->getSession();
        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('UsuarioBundle:Usuario')->find($sesion->get('usuario_id'));
        $formulario = $this->createForm(new UsuarioType(), $usuario);
        if ($peticion->getMethod() == 'POST') {
            $formulario->bind($peticion);
            if ($formulario->isValid()) {
                $em->persist($usuario);
                $em->flush();
                return $this->redirect($this->generateUrl('comunidad'));

            }
        }
        return $this->render('UsuarioBundle:Default:registro.html.twig',
            array(
                'edit'=> true,
                'ruta' => 'edit_user',
                'usuario' => $usuario,
                'formulario' => $formulario->createView()
            ));

    }

    public function comunidadAction()
    {
        $peticion = $this->getRequest();
        $sesion = $peticion->getSession();
        $em = $this->getDoctrine()->getManager();
        $usuario = $this->get('security.context')->getToken()->getUser();
        $usuario_id = $usuario->getId();
        $em->detach($usuario);
        $sesion->set('usuario_id', $usuario->getId());


        $comunidad = $em->getRepository('UsuarioBundle:Comunidad')->findComunidad($usuario_id);
        $em->detach($comunidad);
        $sesion->set('comunidad_id', $comunidad->getId());

        $propietarios = $em->getRepository('UsuarioBundle:Propietario')->findAllPropietarios($comunidad->getId());
        return $this->render('UsuarioBundle:Default:comunidad.html.twig',
            array(
                'edit' => false,
                'comunidad' => $comunidad,
                'propietarios' => $propietarios
            ));
    }

    public function createComunidadAction()
    {
        $peticion = $this->getRequest();
        $usuario = $this->get('security.context')->getToken()->getUser();
        $usuario_id = $usuario->getId();
        $em = $this->getDoctrine()->getManager();
        $comunidad = new Comunidad();
        $comunidad->setUsuario($usuario);
        $formulario = $this->createForm(new ComunidadType(), $comunidad);
        if ($peticion->getMethod() == 'POST') {
            $formulario->bind($peticion);
            // Validar los datos enviados y guardarlos en la base de datos
            if ($formulario->isValid()) {
                $em->persist($comunidad);
                $em->flush();
                return $this->redirect($this->generateUrl('comunidad'));

            }
        }


        return $this->render('UsuarioBundle:Default:comunidad.html.twig',
            array('edit' => false
            ));
    }

    public function editComunidadAction()
    {
        $peticion = $this->getRequest();
        $usuario = $this->get('security.context')->getToken()->getUser();
        $usuario_id = $usuario->getId();
        $em = $this->getDoctrine()->getManager();
        $comunidad = $em->getRepository('UsuarioBundle:Comunidad')->findComunidad($usuario_id);
        $formulario = $this->createForm(new ComunidadType(), $comunidad);
        if ($peticion->getMethod() == 'POST') {

            $formulario->bind($peticion);
            // Validar los datos enviados y guardarlos en la base de datos
            if ($formulario->isValid()) {
                $em->persist($comunidad);
                $em->flush();
                return $this->redirect($this->generateUrl('comunidad'));

            }
        }

        return $this->render('UsuarioBundle:Default:comunidadDetail.html.twig',
            array(
                'edit' => true,
                'comunidad' => $comunidad,
                'formulario' => $formulario->createView()
            ));
    }

    public function createPropietarioAction()
    {
        $em = $this->getDoctrine()->getManager();
        $peticion = $this->getRequest();
        $sesion = $peticion->getSession();

        $propietario = new Propietario();
        //$usuario = $this->get('security.context')->getToken()->getUser();
        $comunidad = $em->getRepository('UsuarioBundle:Comunidad')->find($sesion->get('comunidad_id'));
        $propietario->setComunidad($comunidad);
        $formulario = $this->createForm(new PropietarioType(), $propietario);
        if ($peticion->getMethod() == 'POST') {

            $formulario->bind($peticion);
            // Validar los datos enviados y guardarlos en la base de datos
            if ($formulario->isValid()) {
                $em->persist($propietario);
                $em->flush();
                return $this->redirect($this->generateUrl('comunidad'));

            }
        }
        return $this->render('UsuarioBundle:Default:propietario.html.twig',
            array(
                'ruta' => 'create_propietario',
                'propietario' => $propietario,
                'formulario' => $formulario->createView()
            ));

    }

    public function editPropietarioAction($id)
    {
        $peticion = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $propietario = $em->getRepository('UsuarioBundle:Propietario')->find($id);
        $formulario = $this->createForm(new PropietarioType(), $propietario);

        if ($peticion->getMethod() == 'POST') {

            $formulario->bind($peticion);
            // Validar los datos enviados y guardarlos en la base de datos
            if ($formulario->isValid()) {
                $em->persist($propietario);
                $em->flush();
                return $this->redirect($this->generateUrl('comunidad'));

            }
        }


        return $this->render('UsuarioBundle:Default:propietario.html.twig',
            array(
                'ruta' => 'edit_propietario',
                'propietario' => $propietario,
                'formulario' => $formulario->createView()
            ));

    }

    public function deletePropietarioAction($id)
    {


        return $this->redirect($this->generateUrl('comunidad'));

    }

}
